package com.key.test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.MessageBox;

public class DBSend{
	
	Statement st=null;
	ResultSet rs=null;
	static ArrayList<String> urllinkfatch = new ArrayList<String>();
	static ArrayList<String> msglinksdata = new ArrayList<String>();
	static ArrayList<String> msg_only = new ArrayList<String>();
	static ArrayList<String> urldata = new ArrayList<String>();
	static ArrayList<String> allrecords = new ArrayList<String>();
	static Map allrecordsdata = new HashMap();


		// TODO Auto-generated method stub
		
		 
		   
		    public void insert(String urllist, String id) {
		    	 String	sql = "INSERT INTO allrecords VALUES ('"+urllist+"',"+id+")";
		        try (Connection conn = this.connect();
		                PreparedStatement pstmt = conn.prepareStatement(sql)) {
		         
		            pstmt.executeUpdate();
		        } catch (SQLException e) {
		            System.out.println(e.getMessage());
		        }
		    }
		    
		    
		    
		    
		    public void insertmsg() {
				// TODO Auto-generated method stub
		    	String sql = "insert into msglinks (msg) values (?)";
			        try (Connection conn = this.connect();
			                PreparedStatement ps = conn.prepareStatement(sql)) {
			         
			        	for (String data: GGshell.msglist) {

				    			ps.setString(1, data);
				    		
				    			ps.addBatch(); 
				    			System.out.println("true");
				    		}
				    		ps.executeBatch(); 
			        } catch (SQLException e) {
			            System.out.println(e.getMessage());
			        }
		    	
				
			}
		    
		    
		    public void inser_urltbldata() {
				// TODO Auto-generated method stub
		    	String sql = "insert into urltbl (url) values (?)";
			        try (Connection conn = this.connect();
			                PreparedStatement ps = conn.prepareStatement(sql)) {
			         
			        	for (String data: WriteData.urlpage2) {

				    			ps.setString(1, data);
				    		
				    			ps.addBatch(); 
				    			System.out.println("true");
				    		}
				    		ps.executeBatch(); 
			        } catch (SQLException e) {
			            System.out.println(e.getMessage());
			        }
		    	
				
			}
		    
		    
		    
		    
		    
		    public void uniq_tbldata() {
				// TODO Auto-generated method stub
		    	String sql = "insert into urltbl (url) values (?)";
			        try (Connection conn = this.connect();
			                PreparedStatement ps = conn.prepareStatement(sql)) {
			         
			        	for (String data: ContactThread.uniquevalues) {

				    			ps.setString(1, data);
				    		
				    			ps.addBatch(); 
				    			System.out.println("true");
				    		}
				    		ps.executeBatch(); 
			        } catch (SQLException e) {
			            System.out.println(e.getMessage());
			        }
		    	
				
			}
		    
		    
		  
		    
		    public void select() {
		        //   String sql = "INSERT INTO allrecords(user,msgid) VALUES(?,?)";
		       	String sql= "SELECT * FROM urltbl";
		    
		           try (Connection conn = this.connect();
		           		Statement st = conn.createStatement()) {
		          ResultSet  rs = (ResultSet) st.executeQuery(sql);
		    			   
		   			    while (rs.next()) {
		   			    	
		   			      
		   			        String url2 = rs.getString("url");
		   			      urllinkfatch.add(url2);
		   			        
		   			    }
		   		
		           } catch (SQLException e) {
		               System.out.println(e.getMessage());
		           }
		       }
		
	

	private Connection connect() {
		// GGshell.CurrentjarLocation+"chromedriver"
		String path2 =  GGshell.CurrentjarLocation;
		String path3 = path2.replace("linkdin.jar","");
		String path4 = path3.replace("linkdin.exe","");
		
		String path =  path4+"linkdin.db";
	//	 String url2 = "jdbc:sqlite:/Users/mac/Documents/linkdin.db";
	
		 System.out.println(path);
		 System.out.println(path);
		 String url5 = "jdbc:sqlite:"+path;
		
		 
	//	 System.out.println(url2);
	//	 System.out.println(url);
		
		 // Users/mac/Documents/
	        Connection conn = null;
	        try {
	            conn = DriverManager.getConnection(url5);
	        } catch (SQLException e) {
	            System.out.println(e.getMessage());
	        }
	        return conn;
	}




	public void selectmsg() {
		
		String sql= "SELECT * FROM msglinks";
	    
        try (Connection conn = this.connect();
        		Statement st = conn.createStatement()) {
       ResultSet  rs = (ResultSet) st.executeQuery(sql);
 			   
       while (rs.next()) {
	    	
	        String msg = rs.getString("msg");
	       int id = rs.getInt("id");
	       msglinksdata.add(msg+":"+id);
	   //    msg_only.add(msg);
	        
	    }
       
       System.out.println(msglinksdata);
       System.out.println(msg_only);
      
		
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            
        }
		
	}




	public void select_urltbl_data() {
    String sql= "SELECT * FROM urltbl";
	    
        try (Connection conn = this.connect();
        		Statement st = conn.createStatement()) {
       ResultSet  rs = (ResultSet) st.executeQuery(sql);
 			   
       while (rs.next()) {
			 //     urldata.add(result.getInt("url"));
			    	urldata.add(rs.getString(1)); 
			    }
		
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
		// TODO Auto-generated method stub
		
	}




	public void allrecord_data() {
		  String sql= "SELECT * FROM allrecords";
		    
	        try (Connection conn = this.connect();
	        		Statement st = conn.createStatement()) {
	       ResultSet  rs = (ResultSet) st.executeQuery(sql);
	 			   
	       while (rs.next()) {
			    	
			       int id = rs.getInt("msgid");
			        String msg = rs.getString("user");
			      allrecordsdata.put(msg,id);
			    allrecords.add(msg+","+id);
			        
			    }
	        } catch (SQLException e) {
	            System.out.println(e.getMessage());
	        }
		
		
	}




	public void delete_msg() {
		// TODO Auto-generated method stub
		 String	sql = "delete from msglinks";
	        try (Connection conn = this.connect();
	                PreparedStatement pstmt = conn.prepareStatement(sql)) {
	         
	            pstmt.executeUpdate();
	        } catch (SQLException e) {
	            System.out.println(e.getMessage());
	        }
	        
	        
	        MessageBox messageBox = new MessageBox(GGshell.shell, SWT.ICON_INFORMATION | SWT.OK);
			  messageBox.setText("Message");
			  messageBox.setMessage("All Message Succesfully Deleted From Database ");
			  messageBox.open();
		
	}




	public void delet_url_data() {
		// TODO Auto-generated method stub
		 String	sql = "delete from urltbl";
	        try (Connection conn = this.connect();
	                PreparedStatement pstmt = conn.prepareStatement(sql)) {
	         
	        	 pstmt.executeUpdate();
	        } catch (SQLException e) {
	            System.out.println(e.getMessage());
	        }
	        
	        MessageBox messageBox = new MessageBox(GGshell.shell, SWT.ICON_INFORMATION | SWT.OK);
			  messageBox.setText("Message");
			  messageBox.setMessage("All URL Succesfully Deleted From Database  ");
			  messageBox.open();
		
	}




	public void selectmsgdata() {
	String sql= "SELECT * FROM msglinks";
	    
        try (Connection conn = this.connect();
        		Statement st = conn.createStatement()) {
       ResultSet  rs = (ResultSet) st.executeQuery(sql);
 			   
       while (rs.next()) {
	    	
	        String msg = rs.getString("msg");
	       int id = rs.getInt("id");
	   //    msglinksdata.add(msg+","+id);
	       msg_only.add(msg);
	        
	    }
       
   
       System.out.println(msg_only);
      
		
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            
        }
	}




	public void clear_Sent_History_data() {
		 String	sql = "delete from allrecords";
	        try (Connection conn = this.connect();
	                PreparedStatement pstmt = conn.prepareStatement(sql)) {
	         
	        	 pstmt.executeUpdate();
	        } catch (SQLException e) {
	            System.out.println(e.getMessage());
	        }
	        
	        
	   	  MessageBox messageBox = new MessageBox(GGshell.shell, SWT.ICON_INFORMATION | SWT.OK);
		  messageBox.setText("Message");
		  messageBox.setMessage("All Records Succesfully Deleted From Database Table  ");
		  messageBox.open();
	        
	
		
	}



	
	
	

}
