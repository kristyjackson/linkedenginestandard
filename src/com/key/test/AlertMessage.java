package com.key.test;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Shell;
public class AlertMessage {
	
	public void successMessage(String message)
	{
		  MessageBox messageBox = new MessageBox(GGshell.shell, SWT.ICON_INFORMATION | SWT.OK);
		  messageBox.setText("Success message");
		  messageBox.setMessage(message);
		  messageBox.open();
	}
	public void failureMessage(String code)
	{
		  MessageBox messageBox = new MessageBox(GGshell.shell, SWT.ICON_INFORMATION | SWT.OK);
		  messageBox.setText("Error message");
		  messageBox.setMessage("Something went wrong: please contact administrator: Error code "+code);
		  messageBox.open();
	}
}

