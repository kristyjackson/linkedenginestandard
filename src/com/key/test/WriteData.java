package com.key.test;



import java.io.BufferedReader;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Text;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebElement;

import com.gargoylesoftware.htmlunit.html.HtmlDivision;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlPage;


public class WriteData 
{
	static BufferedWriter bufferWriter=null;
	static FileWriter bufferWriter2=null;
	static BufferedWriter bufferWriter12=null;
	static BufferedWriter bufferWriter1=null ;
	static int total_groups=0;
	
	static  String noOFMember;
	static  String noOFMember2;
	
	static  String noOFMember4;
	static String link;
	static String name;
	
	static ArrayList<String> urlpage = new ArrayList();
	static ArrayList<String> urlpage2 = new ArrayList();
	static ArrayList<String> urlpost = new ArrayList();
	static ArrayList<String> urlpeople = new ArrayList();
	
	static ArrayList<String> urlsearch = new ArrayList();

	static ArrayList<String> mygrouplist = new ArrayList();
	static ArrayList<String> mygroupmemberlist = new ArrayList();
	static HashSet finalgroups=null;
	static int multiTotal=0;
	Element parentElement=null;
	String filenameOG;
	File file1OG;
    static int multiopen=0;
    static int mutiAll=0;
    
    
    static String connection;
    
    
    public void copyandoverwrite(String source,String destination) throws IOException 
	{
		File fin = new File(source);
		FileInputStream fis = new FileInputStream(fin);
		BufferedReader in = new BufferedReader(new InputStreamReader(fis));
		FileWriter fstream = new FileWriter(destination, true);
		BufferedWriter out = new BufferedWriter(fstream);
		String aLine = null;
		while ((aLine = in.readLine()) != null) {
			//Process each line and add output to Dest.txt file
			out.write(aLine);
			out.newLine();
		}
		// do not forget to close the buffer reader
		in.close();
 
		// close buffer writer
		out.close();

}
    
    
    
    
    public void WriteContact1(String html) {
		System.out.println("write");
		   
	       try{
	    	   Document doc2 = Jsoup.parse(html);


	  	     Elements memberelements=doc2.select("[class*=mn-connection-card__details]");
	  	     
	  	     for (Element element : memberelements) {
	  				Element URLandName = element.select("a").first();
//	  				 Elements member2=element.select("[class*=name actor-name]");
//	  				 Elements mem=element.select("[class*=subline-level-1 t-14 t-black t-normal search-result__truncate]");
//	  				
	  				String URL=URLandName.attr("href");

	  				
	  				String content = "https://www.linkedin.com"+URL;	
	  				urlpage2.add(content);
	  				System.out.println(content);			
	  				
	  			}
	                
	     
	       }
	   
	           catch(Exception e ){
	        
	             }
	   
	}

    
    
    public  void Writemembergroup(String html) {
    	
   	 Document doc2 = Jsoup.parse(html);

     Elements memberelements=doc2.select("[class*=pv2 ui-entity-action-row ember-view]");
        
        for (Element element : memberelements) { 
       	 Element URLandName = element.select("a").first();
       	 
       	 try{
       		
       		
       		 
       			String URL=URLandName.attr("href");
       			 String title3 =URLandName.text();
       			
       			
       			
       			mygroupmemberlist.add("https://www.linkedin.com"+URL+","+title3);
       			System.out.println(mygroupmemberlist);
       		
   	    		
   	    		
       	 }
       	 catch(Exception e4){
       		 e4.printStackTrace();
       		 
       		 
       	   }
       }
       	
       	
       	 FileWriter fileWriter;
   	       try{
   	    	   // GGshell.myDocsPath + "//CreateAutomate//LinkedEngine//"+GGshell.email+"//Groups
   	   		//	fileWriter = new FileWriter(GGshell.myDocsPath+"//FBVA//"+GGshell.LoginIDName+"//Groups//"+GGshell.inputkeyname+".csv");
   	        fileWriter = new FileWriter(GGshell.myDocsPath +"//CreateAutomate//LinkedEngine//"+GGshell.email+"//Groups//"+GetMemberThread.name+".csv");
   	       bufferWriter = new BufferedWriter(fileWriter);
   	       } 
   	       catch (Exception exception) 
   	       {
   	        exception.printStackTrace();
   	       }


   	       for (String nameandurl : mygroupmemberlist) 
   	       {
   	        try {
   	         
   	         bufferWriter.write(nameandurl);
   	         bufferWriter.write("\n");
   	        } 
   	        catch (Exception exception) 
   	        {

   	         System.out.println("buffer write problam");
   	        }
   	       }
   	       
   	       try {
   	        bufferWriter.close();
   	       } catch (Exception e) {
   	        // TODO: handle exception
   	       }
   	       
   	       
   	    File temp=new File(GGshell.myDocsPath +"//CreateAutomate//LinkedEngine//"+GGshell.email+"//Groups//Composite_MemberData.csv");
		if (!temp.exists()) {
			try 
			{
				temp.createNewFile();

			} catch (Exception e) 
			{	
				e.printStackTrace();
			}
		}
		try {
			
			copyandoverwrite(GGshell.myDocsPath +"//CreateAutomate//LinkedEngine//"+GGshell.email+"//Groups//"+GetMemberThread.name+".csv",GGshell.myDocsPath +"//CreateAutomate//LinkedEngine//"+GGshell.email+"//Groups//Composite_MemberData.csv");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

   
   	       
       	
       	
        }
    
    
    
 public  void Writemygroup(String html) {
    	
	 Document doc2 = Jsoup.parse(html);

     Elements memberelements=doc2.select("[class*=artdeco-list__item ember-view]");
     
     for (Element element : memberelements) { 
    	 Element URLandName = element.select("a").first();
    	 
    	 try{
    		
    		
    		 
    			String URL=URLandName.attr("href");
    			 String title3 =URLandName.text();
    			
    			
    			
    			mygrouplist.add("https://www.linkedin.com"+URL+","+title3);
    			System.out.println(mygrouplist);
    		
	    		
	    		
    	 }
    	 catch(Exception e4){
    		 e4.printStackTrace();
    		 
    		 
    	   }
    }
    	
    	
    	 FileWriter fileWriter;
	       try{
	    	   // GGshell.myDocsPath + "//CreateAutomate//LinkedEngine//"+GGshell.email+"//Groups
	   		//	fileWriter = new FileWriter(GGshell.myDocsPath+"//FBVA//"+GGshell.LoginIDName+"//Groups//"+GGshell.inputkeyname+".csv");
	        fileWriter = new FileWriter(GGshell.myDocsPath +"//CreateAutomate//LinkedEngine//"+GGshell.email+"//Groups//mygroup.csv");
	        bufferWriter = new BufferedWriter(fileWriter);
	       } 
	       catch (Exception exception) 
	       {
	        exception.printStackTrace();
	       }


	       for (String nameandurl : mygrouplist) 
	       {
	        try {
	         
	         bufferWriter.write(nameandurl);
	         bufferWriter.write("\n");
	        } 
	        catch (Exception exception) 
	        {

	         System.out.println("buffer write problam");
	        }
	       }
	       
	       try {
	        bufferWriter.close();
	       } catch (Exception e) {
	        // TODO: handle exception
	       }
	       
    	
    	
     }
    
    
    
    
    
    
    
    
    
    
    
	
    
    
    
    public  void SearchedPeople(String html) {
    	
    	try {
    		
    		
    		  Document doc2 = Jsoup.parse(html);


    		     Elements memberelements=doc2.select("[class*=search-result__info pt3 pb4 ph0]");   //for people and school
    		     
    		     Elements memberelements2=doc2.select("[class*=search-result__info pt3 pb4 pr5]");// for group
    		      
    		      Elements memberelements3=doc2.select("[class*=search-result__info pt3 pb4 pr0]");// for company
    		     
    		     if(!memberelements.isEmpty()) {
    		    	 System.out.println("++++++++++++++++++++++++++++++++++++");
    		     for (Element element : memberelements) {
    		    	 
    					Element URLandName = element.select("a").first();
    					 Elements member2=element.select("[class*=name actor-name]");
    					
    					 
    					 // this if case for school name
    					  if(!member2.isEmpty()){
    						  Elements member3=element.select("[class*=name actor-name]");
    						  name=member3.text();
    					   
    					      
    					     }
    					  else {
    						   Elements member25=element.select("[class*=search-result__title t-16 t-black t-bold]");
     					      name = member25.text();
    						  
    					  }
    					     
    					
    					
    					String URL=URLandName.attr("href");
    					
    			
    					String content = "https://www.linkedin.com"+URL+","+name;	
    					System.out.println(content);	
    					
    					urlsearch.add(content);
    					
    				}
    		     }
    		     else if(!memberelements2.isEmpty()) {
    		    	 
    		    	   System.out.println("------------------------------");
    		           for (Element element : memberelements2) {
    		         Element URLandName = element.select("a").first();
    		          Elements member2=element.select("[class*=search-result__title t-16 t-black t-bold]");
    		      
    		         
    		         String URL=URLandName.attr("href");
    		         String name=member2.text();
    		      
    		         String content = "https://www.linkedin.com"+URL+","+name; 
    		         System.out.println(content);	
    		     	urlsearch.add(content);
    		         
    		        }
    		    	 
    		    	 
    		     }
    		     else {
    		    	 
    		    	 for (Element element : memberelements3) {
    		    	     Element URLandName = element.select("a").first();
    		    	  
    		    	      Elements member=element.select("[class*=search-result__title t-16 t-black t-bold]");
    		    	     
    		    	     String URL=URLandName.attr("href");
    		    	     String name=member.text();
    		    	     
    		    	     String content = "https://www.linkedin.com"+URL+","+name; 
    	    		     	urlsearch.add(content);
    	    		     	 System.out.println(content);	
    		    	     
    		    	    }
    		    	 
    		     }
    		
    	}
    	catch(Exception e4) {
    		e4.printStackTrace();
    		
    	}
    	
    	
    	 FileWriter fileWriter;
	       try{
	        fileWriter = new FileWriter(GGshell.LocationfileSave);
	        bufferWriter = new BufferedWriter(fileWriter);
	       } 
	       catch (Exception exception) 
	       {
	        exception.printStackTrace();
	       }


	       for (String nameandurl : urlsearch) 
	       {
	        try {
	         
	         bufferWriter.write(nameandurl);
	         bufferWriter.write("\n");
	        } 
	        catch (Exception exception) 
	        {

	         System.out.println("buffer write problam");
	        }
	       }
	       
	       try {
	        bufferWriter.close();
	       } catch (Exception e) {
	        // TODO: handle exception
	       }
	       
    	
    	
    	
    }
    
	
	
    public  void WritePeople(String html) {
    	
    	try {
  	  Document doc2 = Jsoup.parse(html);

  	  Elements memberelements=doc2.select("[class*=mn-discovery-entity-card mn-discovery-entity-card--with-coverphoto ember-view]");
  	  
  	  for (Element element : memberelements) {
  		  
  		  try{
  	 		Element URLandName = element.select("a").first();
  	 		 Elements member2=element.select("[class*= mn-discovery-person-card__name--with-coverphoto t-16 t-black t-bold]");
  	 		 
  	 		 Elements member3=element.select("[class*= mn-discovery-person-card__occupation--with-coverphoto t-14 t-black--light t-normal]");
  	 		 
  	 		 Elements member4=element.select("[class*= member-insights__reason pl1 t-12 t-black--light t-normal]");
  	 		
  	 		
  	 		 
  	 		String URL=URLandName.attr("href");
  	 		String name=member2.text();
  	 		String title=member3.text();
  	 		String name4=member4.text();
  	 		
  	 		if(name4.contains("mutual")){
  	 		  
  	 		  String output  = name4.substring(0,name4.indexOf("m"));
  	 
  	 		connection =output;
  	 
  	 		 }
  	 	
  	 		 else{
  	 		    connection ="0";
  	 		 }
  	 	
  	 		String content = "https://www.linkedin.com"+URL+","+name+","+title+","+connection;	
  	 		System.out.println(content);	
  	 		urlpeople.add(content);
  		  }
  		  catch(Exception e3){
  			  e3.printStackTrace();			  
  		  }
  	 		
  	 	}
    	}
    	catch(Exception e3) {
    		e3.printStackTrace();
    		
    	}
    	
    	
    	
    	 FileWriter fileWriter;
	       try{
	        fileWriter = new FileWriter(GGshell.LocationfileSave);
	        bufferWriter = new BufferedWriter(fileWriter);
	       } 
	       catch (Exception exception) 
	       {
	        exception.printStackTrace();
	       }


	       for (String nameandurl : urlpeople) 
	       {
	        try {
	         
	         bufferWriter.write(nameandurl);
	         bufferWriter.write("\n");
	        } 
	        catch (Exception exception) 
	        {

	         System.out.println("buffer write problam");
	        }
	       }
	       
	       try {
	        bufferWriter.close();
	       } catch (Exception e) {
	        // TODO: handle exception
	       }
	       
	       
	       
	       
	       
  	    	
  	    		
  				
  			}
	



	public void WriteContact(String html) {
		System.out.println("write");
		   
	       try{
	    	   Document doc2 = Jsoup.parse(html);


	    	   Elements memberelements=doc2.select("[class*=mn-connection-card__details]");
	  	     
	  	     for (Element element : memberelements) {
	  				Element URLandName = element.select("a").first();
	  				 Elements member2=element.select("[class*=mn-connection-card__name t-16 t-black t-bold]");
	  				 Elements mem=element.select("[class*=mn-connection-card__occupation t-14 t-black--light t-normal]");
	  				
	  				String URL=URLandName.attr("href");
	  				String name=member2.text();
	  				String title2 = mem.text();
	  				String title = title2.replace(",", " ");
	  				
	  				String content = "https://www.linkedin.com"+URL+","+name+","+title;	
	  				urlpage.add(content);
	  				System.out.println(content);			
	  				
	  			}
	                
	               
	         
	      
	  
	       }
	   
	           catch(Exception e ){
	        
	             }
	       
	       FileWriter fileWriter;
	       try{
	        fileWriter = new FileWriter(GGshell.LocationfileSave);
	        bufferWriter = new BufferedWriter(fileWriter);
	       } 
	       catch (Exception exception) 
	       {
	        exception.printStackTrace();
	       }


	       for (String nameandurl : urlpage) 
	       {
	        try {
	         
	         bufferWriter.write(nameandurl);
	         bufferWriter.write("\n");
	        } 
	        catch (Exception exception) 
	        {

	         System.out.println("buffer write problam");
	        }
	       }
	       
	       try {
	        bufferWriter.close();
	       } catch (Exception e) {
	        // TODO: handle exception
	       }
	       

		
	}
	

}