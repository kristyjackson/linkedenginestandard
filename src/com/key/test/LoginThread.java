package com.key.test;
import java.io.File;



import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Rectangle;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.MessageBox;
import org.eclipse.swt.widgets.Monitor;
import org.eclipse.swt.widgets.ProgressBar;
import org.eclipse.swt.widgets.Shell;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.html.HtmlPasswordInput;
import com.gargoylesoftware.htmlunit.html.HtmlSubmitInput;
import com.gargoylesoftware.htmlunit.html.HtmlTextInput;





public class LoginThread implements Runnable {
	static ChromeDriver chromedriver;

    static String id = null,pass = null;
    @Override
    public void run() {
       
    //    GenericFunctions  gf=new GenericFunctions();;
        if(!GGshell.email.isEmpty()&&!GGshell.password.isEmpty())
        {
           
        	   id = GGshell.email;
               pass = GGshell.password;  
     
   
       

//            GGshell.vpojo.setLoginalertdone(false);
//            GGshell.vpojo.setMyGroupGGMessageToBeSent(false);
//            GGshell.vpojo.setMyGroupGGMessageSent(false);
               
               
           	new File(GGshell.myDocsPath + "//CreateAutomate//LinkedEngine//"+GGshell.email+"//Groups").mkdirs();
   // 		new File(GGshell.myDocsPath + "//CreateAutomate//LinkedEngine"+GGshell.email+"//People").mkdirs();
    
       
        try{
           
         /*   GGshell.htmlunitwebClient= new WebClient(BrowserVersion.FIREFOX_24);
            GGshell.htmlunitwebClient.setAjaxController(new NicelyResynchronizingAjaxController());
            GGshell.htmlunitwebClient.getOptions().setThrowExceptionOnFailingStatusCode(false);
            GGshell.htmlunitwebClient.getOptions().setThrowExceptionOnScriptError(false);
            GGshell.htmlunitwebClient.waitForBackgroundJavaScript(50000);
            GGshell.htmlunitwebClient.setJavaScriptTimeout(0);
             
            HtmlPage login = GGshell.htmlunitwebClient.getPage("https://www.facebook.com/");
            final HtmlForm form = login.getForms().get(0);
            final HtmlSubmitInput button = form.getInputByValue("Log In");
            final HtmlTextInput textField = form.getInputByName("email");
            final HtmlPasswordInput pwd = form.getInputByName("pass");
            textField.setValueAttribute(id);
            pwd.setValueAttribute(password);
   
            GGshell.loggedinpage= (HtmlPage) button.click();
            System.out.println( GGshell.loggedinpage.asText());  
            */
            
            //Phantom js login
           
             GGshell.caps = new DesiredCapabilities();
            GGshell.caps.setJavascriptEnabled(true);
            GGshell.caps.firefox();
           
     
        	
            System.setProperty("webdriver.chrome.driver",
					"chromedriver.exe");
			Map<String, Object> prefs = new HashMap<String, Object>();
			prefs.put(
					"profile.default_content_setting_values.notifications",
					2);
			ChromeOptions options = new ChromeOptions();
	//		 options.addArguments("headless");
			options.setExperimentalOption("prefs", prefs);
			chromedriver = new ChromeDriver(options);
			
			 
          
           
            System.out.println("111111111111111111111111111");
           chromedriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
         chromedriver.get("https://www.linkedin.com/");
    
         try{
           chromedriver.findElement(By.id("login-email")).sendKeys(id);
           chromedriver.findElement(By.id("login-password")).sendKeys(pass);
           chromedriver.findElement(By.id("login-submit")).click();
         }
         catch(Exception e2){
        	 chromedriver.findElement(By.id("login-email")).sendKeys(id);
             chromedriver.findElement(By.id("login-password")).sendKeys(pass);
             chromedriver.findElement(By.id("login-submit")).click();
        	 e2.printStackTrace();
        	 
         }
          
            System.out.println("successfull");

          
            
            
            
        }
        catch(Exception ex)
        { 
           
            Display.getDefault().asyncExec(new Runnable() {
               
                @Override
                public void run() {
                 /*   GGshell.shellMessage.setVisible(false);
                    GGshell.vpojo.setProgressLoginBoolean(Boolean.FALSE);
                    AlertMessage amessage=new AlertMessage();
                    amessage.failureMessage("001");
                     System.out.println("i am exception " +ex);*/
                   
                   
                }
            });
          
        }



            Display.getDefault().asyncExec(new Runnable() {
               
                @Override
                public void run() {
                    //GGshell.browser1.setUrl("https://www.facebook.com/");
//                    AlertThreadLogin ltlogin=new AlertThreadLogin();
//                      Thread t1 =new Thread(ltlogin); 
////                           t1.start();
//                           GGshell.shellMessage.setVisible(false);
//                        GGshell.vpojo.setSuceessfullLogin(Boolean.FALSE );
                        GGshell.lblStatus.setText("Login");
                        GGshell.vpojo.setSuceessfullLogin(Boolean.TRUE );
                     
                   
                }
            });
               
   
    }
       
        else
    {
            Display.getDefault().asyncExec(new Runnable() {
               
                @Override
                public void run() {
           //        GGshell.shell.setVisible(false);
                     MessageBox messageBox = new MessageBox(GGshell.shell, SWT.ICON_WARNING | SWT.OK);
                      messageBox.setText("Warning");
                      messageBox.setMessage("Please provide Email id & Password");
                      messageBox.open();
                               
                }
            });
    }

}


    }