package com.key.test;
 
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Spinner;
 
public class SpinnerDemo {
 
    public static void main(String[] args) {
 
        Display display = new Display();
        Shell shell = new Shell(display);
        shell.setText("SWT Spinner (o7planning.org)");
 
        RowLayout rowLayout = new RowLayout();
        rowLayout.marginLeft = 10;
        rowLayout.marginTop = 10;
        rowLayout.spacing = 15;
        shell.setLayout(rowLayout);
 
        // Label
        Label label = new Label(shell, SWT.NONE);
        label.setText("Select Level:");
 
        // Spinner
        final Spinner spinner = new Spinner(shell, SWT.BORDER);
        spinner.setIncrement(5);
        spinner.setMinimum(5);
        spinner.setMaximum(500);
        spinner.setSelection(5);
 
        Font font = new Font(display, "Courier", 20, SWT.NORMAL);
        spinner.setFont(font);
        
        
        final Spinner spinner2 = new Spinner(shell, SWT.BORDER);
    
        spinner2.setMinimum(1);
        spinner2.setMaximum(100);
        spinner2.setSelection(3);
 
        Font font2 = new Font(display, "Courier", 20, SWT.NORMAL);
        spinner2.setFont(font2);
        
        
        
        final Spinner spinner3 = new Spinner(shell, SWT.BORDER);
        spinner3.setIncrement(5);
        spinner3.setMinimum(5);
        spinner3.setMaximum(100);
        spinner3.setSelection(5);
 
        Font font3 = new Font(display, "Courier", 20, SWT.NORMAL);
        spinner3.setFont(font3);
        
        
        final Spinner spinner4 = new Spinner(shell, SWT.BORDER);
        spinner4.setIncrement(5);
        spinner4.setMinimum(15);
        spinner4.setMaximum(100);
        spinner4.setSelection(15);
       
 
        Font font4 = new Font(display, "Courier", 20, SWT.NORMAL);
        spinner4.setFont(font4);
        
        
 
        // Label
        Label labelMsg = new Label(shell, SWT.NONE);
        labelMsg.setText("?");
        labelMsg.setLayoutData(new RowData(150, SWT.DEFAULT));
        
 
        spinner.addSelectionListener(new SelectionAdapter() {
 
            @Override
            public void widgetSelected(SelectionEvent e) {
                labelMsg.setText("You select: " + spinner.getSelection());
            }
        });
        
        
        spinner2.addSelectionListener(new SelectionAdapter() {
        	 
            @Override
            public void widgetSelected(SelectionEvent e) {
                labelMsg.setText("You select: " + spinner.getSelection());
            }
        });
        
        
        
        spinner3.addSelectionListener(new SelectionAdapter() {
        	 
            @Override
            public void widgetSelected(SelectionEvent e) {
                labelMsg.setText("You select: " + spinner.getSelection());
            }
        });
        
        
        spinner4.addSelectionListener(new SelectionAdapter() {
        	 
            @Override
            public void widgetSelected(SelectionEvent e) {
                labelMsg.setText("You select: " + spinner.getSelection());
            }
        });
 
 
 
 
        shell.setSize(400, 250);
        shell.open();
        while (!shell.isDisposed()) {
            if (!display.readAndDispatch())
                display.sleep();
        }
        display.dispose();
    }
 
}
